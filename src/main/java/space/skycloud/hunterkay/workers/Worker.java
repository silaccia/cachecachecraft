package space.skycloud.hunterkay.workers;

import org.bukkit.entity.Player;
import space.skycloud.hunterkay.CacheCacheCraft;
import space.skycloud.hunterkay.beans.CacheCacheParty;

/**
 * Created by sky on 26.10.15.
 */
public class Worker {

    public boolean createCacheCache(Player owner, boolean isPrivate){
        CacheCacheParty party = new CacheCacheParty(owner, isPrivate);
        return CacheCacheCraft.addParty(party);
    }
}
