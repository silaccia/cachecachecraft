package space.skycloud.hunterkay.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import space.skycloud.hunterkay.controllers.Controller;

/**
 * Created by sky on 26.10.15.
 */
public class CCCCommands implements CommandExecutor{
    private Controller partyCtrl;

    public CCCCommands(Controller partyCtrl) {
        this.partyCtrl = partyCtrl;
    }

    // /ccc create private
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(args.length == 0){
            return false;
        }
        String arg1 = args[0].toLowerCase();
        switch(arg1){
            case "create":
                if(sender.hasPermission("ccc.create")){
                    if(args.length > 1 && args[1].equalsIgnoreCase("private")){
                        if(sender.hasPermission("ccc.create.private")){
                            createCacheCache((Player) sender, true);
                            break;
                        }else{
                            return false;
                        }
                    }
                    createCacheCache((Player) sender, false);
                }
                break;
            case "stop":
                break;
        }
        return true;
    }

    private void createCacheCache(Player owner, boolean isPrivate){
        boolean created = partyCtrl.createCacheCache(owner, isPrivate);
        if(created){
            owner.sendMessage("CCC: Votre partie a bien ete cree :)");
        }else{
            owner.sendMessage("CCC: Desole imposible de creer la partie");
        }
    }
}
