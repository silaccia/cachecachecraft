package space.skycloud.hunterkay.controllers;

import org.bukkit.entity.Player;
import space.skycloud.hunterkay.workers.Worker;

/**
 * Created by sky on 26.10.15.
 */
public class Controller {
    private Worker gameWrk;

    public boolean createCacheCache(Player owner, boolean isPrivate){
        if(this.gameWrk != null){
            return this.gameWrk.createCacheCache(owner, isPrivate);
        }
        return false;
    }

    public void setGameWrk(Worker gameWrk) {
        this.gameWrk = gameWrk;
    }
}
