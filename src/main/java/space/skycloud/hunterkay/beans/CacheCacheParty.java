package space.skycloud.hunterkay.beans;

import org.bukkit.entity.Player;

/**
 * Created by sky on 26.10.15.
 */
public class CacheCacheParty {
    private Player owner;
    private Player[] players;
    private boolean isPrivate;
    private int indexPlayers;

    public CacheCacheParty(Player owner, boolean isPrivate) {
        this.owner = owner;
        this.isPrivate = isPrivate;
        this.players = new Player[9];
    }

    public boolean joinParty(Player p){
        if(indexPlayers < players.length){
            this.players[indexPlayers++] = p;
            return true;
        }
        return false;
    }

}
