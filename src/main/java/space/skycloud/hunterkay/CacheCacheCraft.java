package space.skycloud.hunterkay;

import org.bukkit.plugin.java.JavaPlugin;
import space.skycloud.hunterkay.beans.CacheCacheParty;
import space.skycloud.hunterkay.commands.CCCCommands;
import space.skycloud.hunterkay.controllers.Controller;
import space.skycloud.hunterkay.workers.Worker;

/**
 * Created by sky on 26.10.15.
 */
public class CacheCacheCraft extends JavaPlugin{
    private static CacheCacheParty[] currentParties = new CacheCacheParty[5];
    private static int indexParties;

    @Override
    public void onDisable() {
        super.onDisable();
    }

    @Override
    public void onEnable() {
        Worker gameWrk = new Worker();
        Controller gameCtrl = new Controller();
        gameCtrl.setGameWrk(gameWrk);
        getCommand("ccc").setExecutor(new CCCCommands(gameCtrl));
    }

    public static boolean addParty(CacheCacheParty party){
        if(indexParties < currentParties.length){
            currentParties[indexParties++] = party;
            return true;
        }
        return false;
    }
}
